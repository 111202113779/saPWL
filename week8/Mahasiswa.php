<?php 

class Mahasiswa {

    public $nama;
    public $nim;
    public $alamat;
    public $prodi;

    function ambil_data($nama, $nim, $alamat, $prodi) {
        $this->nama = $nama;
        $this->nim = $nim;
        $this->alamat = $alamat;
        $this->prodi = $prodi;
    }

    function tampil() {
        // NAMA
        if(!$this->nama) {
            echo "<script>
                alert('Nama tidak boleh kosong!')
                document.location.href = 'form_mahasiswa.php';
            </script>";
        }
        else
            echo "Nama : ".$this->nama."<br>";
        // NIM
        if(!$this->nim) {
            echo "<script>
                alert('NIM tidak boleh kosong!')
                document.location.href = 'form_mahasiswa.php';
            </script>";
        }
        else
            echo "NIM : ".$this->nim."<br>";
        if(!$this->alamat) {
            echo "<script>
                alert('Alamat tidak boleh kosong!')
                document.location.href = 'form_mahasiswa.php';
            </script>";
        }
        else
            echo "Alamat : ".$this->alamat."<br>";

        // prodi
        if($this->prodi == "A11")
            echo "Prodi : Teknik Informatika<br>";
        else if($this->prodi == "A12")
            echo "Prodi : Sistem Informasi<br>";
        else if($this->prodi == "B11")
            echo "Prodi : Management<br>";
        else if($this->prodi == "B12")
            echo "Prodi : Akuntansi<br>";
        else {
            echo "<script>
                alert('Data yang anda masukan tidak valid!')
                document.location.href = 'form_mahasiswa.php';
            </script>";
        }
    }
}