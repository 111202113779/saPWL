<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Mahasiswa</title>
</head>
<body>
    <form action="aksi_mahasiswa.php" method="POST">
        <input type="text" name="nama" placeholder="Masukan Nama Lengkap">
        <input type="text" name="nim" placeholder="Masukan NIM">
        <input type="text" name="alamat" placeholder="Alamat">
        <select name="prodi">
            <option>Pilih Prodi</option>
            <option value="A11">Teknik Informatika</option>
            <option value="A12">Sistem Informasi</option>
            <option value="B11">Management</option>
            <option value="B12">Akuntansi</option>
        </select>
        <button type="submit">submit</button>
    </form>
</body>
</html>