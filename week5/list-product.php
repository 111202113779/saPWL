<!DOCTYPE html>
<html lang="en">
<head>
    <?php
        session_start();
        if (empty($_SESSION['cart']["arrCart"]))
            $_SESSION['cart']["arrCart"]=array(); 					
    ?>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <title>Document</title>
    <style>
        .text-light{
            text-decoration: none;
        }
        .barang {
            width: 250px;
            height:50px;
        }
        .justify-content-start {
            gap: 10px;
        }
        .img {
            position: absolute;
            right: 3rem;
            top: 10rem;
            width: 250px;
            background-image: none;
        }
    </style>
</head>
<body class="bg-dark">
    <img src="img/anim1.png" class="img">
    <div class="container">
        <h3 class="text-light mb-3 mt-3">Daftar Buku</h3>
        <div class="d-flex flex-column mb-3 ">
            <h5 class="text-light">Kumpulan Novel</h5>
            <div class="p-2 d-flex justify-content-start">
                <img src="img/Girls in The Dark.png" width="50px">
                <div class="barang  d-flex flex-column mb-3">
                    <p class="text-light">Girls in The Dark : 99000</p>
                    <a href="addCart.php?brg=Girls+in+The+Dark&hrg=99000&jml=1" class="btn btn-success">Price</a>
                </div>
            </div>
            <div class="p-2 d-flex justify-content-start">
                <img src="img/Kill Me If You Can.png" width="50px">
                <div class="barang  d-flex flex-column mb-3">
                    <p class="text-light">Kill Me If You Can : 90000</p>
                    <a href="addCart.php?brg=Kill+Me+If+You+Can&hrg=90000&jml=1" class="btn btn-success">Price</a>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column mb-3 ">
            <h5 class="text-light">Kumpulan Komik</h5>
            <div class="p-2 d-flex justify-content-start">
                <img src="img/komik-one.jpg" width="50px">
                <div class="barang  d-flex flex-column mb-3">
                    <p class="text-light">One Piece : 85000</p>
                    <a href="addCart.php?brg=One+Piece&hrg=85000&jml=1" class="btn btn-success">Price</a>
                </div>
            </div>
            <div class="p-2 d-flex justify-content-start">
                <img src="img/komik-spy.jpg" width="50px">
                <div class="barang d-flex flex-column mb-3">
                    <p class="text-light">Spy x Family : 88000</p>
                    <a href="addCart.php?brg=Spy+x+Family&hrg=88000&jml=1" class="btn btn-success">Price</a>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>
</html>