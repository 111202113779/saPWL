<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- ECHO -->
    <?php
        $teks="Ali Ma’ruf senang belajar PHP!<br>";
        echo "<h2>PHP mudah digunakan</h2>";
        echo ("Hello world!<br>");
        echo $teks;
        echo "teks", "ini", "dibuat","dengan","banyak parameter";
    ?> <br>
    <!-- PRINT -->
    <?php 
        $txt1 = "Belajar PHP";
        $txt2 = "Fakultas Ilmu Komputer";
        $x = 5;
        $y = 4;
        $ret=print ("Hello World");
        print "nilai return = $ret";
        print "<h2>".$txt1."</h2>";
        print "Belajar  Pemrograman Web  di ".$txt2."<br>";
        print $x + $y;
    ?><br>
    <!-- PRINT_R -->
    <?php 
        $siswa = array ('Arif', 'Beta', 'Cici'); 
        echo '<pre>'; 
        print_r($siswa); 
        echo '</pre>';    
    ?><br>
    <!-- VAR_DUMP() -->
    <?php
        $nama = 'Agus';
        var_dump ($nama); // Hasil: string(4) "Agus“

        $siswa = array(
            'nama' => array ('Arif', 'Beta', 'Cici'),
            'jurusan' 	=> 'Informatika',
            'semester'=> 1,
            1 => 'Jakarta',
            2 => 'Surabaya'
            );
                
        echo '<pre>';  var_dump($siswa); echo '</pre>';
    ?>
    <!-- COMMAND -->
    <?php
        //ini teks yg tdk dieksekusi oleh engine php
        /*
        Ini adalah blok 
        baris 
        komentar 
        */
    ?>
    <!-- VARIABLE LOCAL-->
    <?php
        function fungsi1() {
        $x = 5; // local scope
        echo "<p>Variable x  di dlm fungsi: $x</p>";
        } 
        fungsi1();
        // menggunakan  variabel x di luar fungsi  akan mengakibatkan error
        echo "<p>Variable x di luar fungsi : $x</p>";
    ?>
    <!-- VARIABLE GLOBAL -->
    <?php
        $x=5;
        $y=10;
        function fungsi2() {
            $GLOBALS['y'] = $GLOBALS['x'] + $GLOBALS['y'];
            // $y = $x + $y;
        }
        fungsi2();
            echo $y; // hasil = 15
    ?><br>
    <!-- VARIABLE STATIK -->
    <?php
        function fungsi3() {
            static $x = 0;
            echo $x;
            $x++;
        }
        fungsi3();
        fungsi3();
        fungsi3();
    ?><br>
    <!-- TYOE DATA STRING-->
    <?php
        $x = "Hello world!";
        $y = 'Hello world!';
        echo $x;
        echo "<br>"; echo $y;
    ?>
    <!-- TYPE DATA INTERGER -->
    <?php
        $x = 5985; 
        var_dump($x);
    ?>
    <!-- TYPE DATA FLOAT -->
    <?php 
        $x = 10.365;
        var_dump($x);
    ?><br>
    <!-- TYPE DATA BOOLEAN -->
    <?php 
        $x = true;
        $y = false;
        var_dump($x);
    ?><br>
    <?php
        $mobil = array("Mitsubishi","Daihatsu","Toyota");
        var_dump($mobil);
    ?><br>
    <!-- TYPE DATA OBJECT -->
    <?php 
        class Car {
            public $color;
            public $model;
            public function __construct($color, $model) {
                $this->color = $color;
                $this->model = $model;
            }
            public function message() {
                return "My car is a ". $this->color ." ".$this->model ."!";
            }
        }
        $myCar = new Car("black", "Volvo");
        echo $myCar -> message();
        echo "<br>";
        $myCar = new Car("red", "Toyota");
        echo $myCar -> message();
    ?><br>
    <!-- TYPE DATA NULL -->
    <?php
        $x = "Hello world!";
        $x = null;
        var_dump($x);
    ?><br>
    <!-- TYPE DATA RESOURCE-->
    <?php 
        $conn = mysqli_connect('localhost','root','','novelku');
        $fp = fopen("tugas1.php",'r');
    ?>
    <!-- TYPE DATA KONSTANTA -->
    <?php 
        define("SALAM", "Selamat Pagi");
        define("mobil", ["Honda", "Daihatsu", "Toyota"]);
        function fungsi4() {
            echo SALAM;
        }
        fungsi4();
        echo mobil[0];
    ?>

</body>
</html>