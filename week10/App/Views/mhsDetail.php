<html>
<head>
    <title>Native MVC Example</title>
    <link rel="stylesheet" href="/phpdasar/PWL/saPWL/week10/assets/css/bootstrap.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

</head>
<body>
    <div class="row">
		<div class="col-md-12 mt-5">
			<div class="col-md-4 mt-5">&nbsp;</div>
            <div class="card col-md-4 mt-5 text-light bg-dark">
                <div class="card-header">
                    <h3>Data Mahasiswa</h3>
                </div>
                <div class="card-body">
                    <h4 class="card-text"><?= 'id: '. $rs['id'] . '<br/>' ?></h4>
                    <h4 class="card-text"><?= 'NIM: ' . $rs['nim'] . '<br/>' ?></h4>
                    <h4 class="card-text"><?= 'Nama: ' . $rs['nama'] . '<br/>' ?></h4>
                    <a href="/phpdasar/PWL/saPWL/week10/?act=tampil-data" class="card-link" style="font-size:12px;">Kembali</a>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/phpdasar/PWL/saPWL/week10/assets/js/bootstrap.js"></script>
</body>
</html>