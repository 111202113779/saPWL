<html>
<head>
    <title>Native MVC Example</title>
    <link rel="stylesheet" href="phpdasar/PWL/saPWL/week10/assets/css/bootstrap.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>
<body>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">&nbsp;</div>
            <div class="container">
                <div class="d-flex justify-content-between">
                    <h3>Data Mahasiswa</h3>
                    <a href="/phpdasar/PWL/saPWL/week10/?act=home" class="btn btn-primary">Tambahkan Data</a>
                </div>
                <br>
                <table class="table table-dark table-sm table-hover">
                    <tr>
                        <td>No</td>
                        <td>NIM</td>
                        <td>Nama</td>
                        <td>Aksi</td>
                    </tr>
                    <?php $i=1; foreach ($rs as $mahasiswa => $list) : ?>
                        <tr>
                            <td><a href="?act=tampil-data&i=<?= $list['id']; ?>"><?= $i++ ?></a></td>
                            <td><?= $list['nim']; ?></td>
                            <td><?= $list['nama']; ?></td>
                            <td>
                                <a href="?act=edit&i=<?=$list['id']?>">Edit</a> |
                                <a href="?act=hapus&i=<?=$list['id']?>">Hapus</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <div class="col-md-4">&nbsp;</div>
        </div>
    </div>
    <script type="text/javascript" src="phpdasar/PWL/saPWL/week10/assets/js/bootstrap.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    
</body>
</html>