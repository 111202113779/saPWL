<html>
<head>
    <title>Native MVC Example</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>
<body>
    <div class="container ms-5 ">
        <div class="ms-5">
            <div class="ms-5">&nbsp;</div>
            <div class="ms-5"><h3 class="ms-5">Input Data Anda</h3>
            <form method="post" action="/phpdasar/PWL/saPWL/week10/?act=simpan" class="ms-5">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="exampleInputNim" placeholder="ext: A11.2000.0001" required name="nim">
                        <label for="exampleInputNim">NIM</label>
                    </div>
                    <div class="form-floating">
                        <input type="text" class="form-control" id="exampleInputNama" placeholder="Nama Lengkap" required name="nama">
                        <label for="exampleInputNama">Nama Lengkap</label>
                    </div> <br>
                    <button type="submit" class="btn btn-secondary">Submit</button>
                </form>
                <br/>
                <a href="/phpdasar/PWL/saPWL/week10/?act=tampil-data" class="ms-5">Data Inputan</a>
            </div>
            <div class="col-md-4">&nbsp;</div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>



<!-- <div class="form-group">
    <label for="exampleInputNim">NIM</label>
    <input type="text" class="form-control" id="exampleInputNim" name="nim" placeholder="NIM">
</div>
<div class="form-group">
    <label for="exampleInputNama">Nama</label>
    <input type="text" class="form-control" id="exampleInputNama" name="nama" placeholder="Nama">
</div> -->