<?php 
namespace Models;
use Libraries\Database;

class Model_mhs {
    public function __construct() {
        $db = new Database();
        $this->dbh = $db->getInstance();
    }

    function simpanData($nim,$nama) {
        $rs = $this->dbh->prepare("INSERT INTO mhs (nim,nama) VALUES (?,?)");
        $rs->execute([$nim,$nama]);
    }

    function lihatData() {
        $rs = $this->dbh->query("SELECT * FROM mhs");
        return $rs;
    }

    function lihatDataDetail($id) {
        $rs = $this->dbh->prepare("SELECT * FROM mhs WHERE id=?");
        $rs->execute([$id]);
        return $rs->fetch();// kalau hasil query hanya satu, gunakan method fetch() bawaan PDO
    }

    function editData($nim,$nama,$id) {
        $rs = $this->dbh->prepare("UPDATE mhs SET nim=?, nama=? WHERE id=?");
        $rs->execute([$nim,$nama,$id]);
    }

    function hapusData($id) {
        $rs = $this->dbh->prepare("DELETE FROM mhs WHERE id=?");
        $rs->execute([$id]);
    }

}